# Start Wizard for Movit Setup
roslaunch moveit_setup_assistant setup_assistant.launch

# Start Moving with GUI 
roslaunch movit_package_i_created_before demo.launch

# Adjust window
wmctrl -r :ACTIVE: -e 0,65,24,1500,550